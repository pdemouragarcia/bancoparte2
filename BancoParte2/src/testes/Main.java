package testes;

import dao.DAOBanco;
import dao.DAOCliente;
import dao.DAOConta;
import view.InformeJuros;
import view.ListarDeletarClienteFisico;
import view.ListarDeletarClienteJuridico;
import view.ListarDeletarContaCorrente;
import view.Principal;

public class Main {

	public static void executarPrimeiraVez() {
		DAOCliente dao = new DAOCliente();
		dao.escreveVetorClienteFisico();
		dao.cadastraCliente("Joao", "Rua 2", "97542100", 12341234, 8000.00, 123456, 121212121);
		dao.cadastraCliente("Maria", "Rua 1", "95542170", 43241234, 5000.00, 123123123, 341212112);
		dao.cadastraCliente("Paulo", "Rua 3", "67542120", 56741234, 3000.00, 789123123, 561212212);
		dao.cadastraCliente("Roberto", "Rua 9", "37542100", 89041234, 7000.00, 357123123, 781121212);
		dao.cadastraCliente("Ana", "Rua 6", "17542160", 35841234, 1000.00, 246123123, 901212212);

		dao.escreveVetorClienteJuridico();
		dao.cadastraPessoaJuridica("CAAL", "Rua 2", "97542100", 12341234, 8000.00, 123456);
		dao.cadastraPessoaJuridica("Peruzzo", "Rua 1", "95542170", 43241234, 5000.00, 123123123);
		dao.cadastraPessoaJuridica("Nacional", "Rua 3", "67542120", 56741234, 3000.00, 789123123);
		dao.cadastraPessoaJuridica("Martini", "Rua 9", "37542100", 89041234, 7000.00, 357123123);
		dao.cadastraPessoaJuridica("Roots", "Rua 6", "17542160", 35841234, 1000.00, 246123123);

		DAOConta daoc = new DAOConta();
		daoc.escreveVetorContaCorrente();
		daoc.criaContaCorrente(1000, true, 1000, null, 123456, "12312312312");
		daoc.criaContaCorrente(1001, true, 1500, null, 123456, "45612312312");
		daoc.criaContaCorrente(1002, true, 2000, null, 123456, "78912312312");
		daoc.criaContaCorrente(1003, true, 2500, null, 123456, "12345678912");
		daoc.criaContaCorrente(1004, true, 3000, null, 123456, "98765432312");

		daoc.escreveVetorContaEspecial();
		daoc.criaContaEspecial(2001, true, 5000, null, 123456, null, 3000);
		daoc.criaContaEspecial(2002, true, 4000, null, 123456, null, 4000);
		daoc.criaContaEspecial(2003, true, 3000, null, 123456, null, 5000);
		daoc.criaContaEspecial(2004, true, 2000, null, 123456, null, 1000);
		daoc.criaContaEspecial(2005, true, 1000, null, 123456, null, 2000);

		daoc.escreveVetorContaPoupanca();
		daoc.criaContaPoupanca(3001, true, 3000, null, 123456, null);
		daoc.criaContaPoupanca(3002, true, 2500, null, 123456, null);
		daoc.criaContaPoupanca(3003, true, 2000, null, 123456, null);
		daoc.criaContaPoupanca(3004, true, 1500, null, 123456, null);
		daoc.criaContaPoupanca(3005, true, 1000, null, 123456, null);

	}

	public static void main(String args[]) {
		//executarPrimeiraVez();
		DAOBanco opera = new DAOBanco();
		opera.carregaArrays();
		//DAOConta dao = new DAOConta();
		//DAOCliente daoo = new DAOCliente();
		
	  //  daoo.imprimeArrayClienteFisico();
		//dao.imprimeArrayContaCorrente(); //puta bug pra imprimir as paradas e to com sono, ve se tu consegue resolver iso que o resto fica facil
		//new teste();
		
		new InformeJuros();
		//new Principal();
		//new ListarEDeletarClienteJuridico();
		//new ListarEDeletarClienteFisico();
		//new ListarDeletarContaCorrente();
	    //new ListaERemoveCliente();
		//new CadastroCliente();
		//new CadastroConta();
		
	}

}
