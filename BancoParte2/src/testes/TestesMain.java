package testes;

import java.util.List;

import dao.DAOBanco;
import dao.ListaLigada;
import dao.Movimento;
import model.ContaCorrente;

public class TestesMain {
	public static void main(String[] args) {
		DAOBanco db = new DAOBanco();
		
		db.carregaArrays();
		
		for(int i = 0; i < db.retornaArrayContasCorrentes().size() ; i++ ){
			System.out.println(db.retornaArrayContasCorrentes().get(i).getNumeroConta());
			db.retornaArrayContasCorrentes().get(i).getMovimentos().exibeItens();
		}

		//db.sacaDaContaCorrente(100, 123456, 156682);
		//db.depositaDaContaCorrente(400, 123456, 156682);
		//db.sacaDaContaCorrente(100, 123456, 167595);
		//db.depositaDaContaCorrente(400, 123456, 167595);
		//db.sacaDaContaCorrente(100, 123456, 194454);
		//db.depositaDaContaCorrente(400, 123456, 194454);
		
		//System.out.println("------------------");
		//System.out.println("Fim do Saque");
		//System.out.println("------------------");
		//db.depositaDaContaCorrente(400, 123456, 130029);
		
		

		/*

		ListaLigada ll = new ListaLigada();

		Movimento mov1 = new Movimento(100, "Saque");
		Movimento mov2 = new Movimento(200, "Deposito");
		Movimento mov3 = new Movimento(200, "Saque");
		Movimento mov4 = new Movimento(500, "Deposito");
		Movimento mov5 = new Movimento(800, "Saque");
		Movimento mov6 = new Movimento(300, "Deposito");
		Movimento mov7 = new Movimento(1200, "Saque");
		Movimento mov8 = new Movimento(4500, "Deposito");
		Movimento mov9 = new Movimento(700, "Saque");

		System.out.println("-------------------------------------------");
		System.out.println(" Retorna Lista vazia ");
		System.out.println("-------------------------------------------");
		ll.exibeItens();
		System.out.println("-------------------------------------------");
		System.out.println(" Inserindo 3 movimentos no inicio ");
		System.out.println("-------------------------------------------");
		ll.insereInicio(mov1);
		ll.insereInicio(mov2);
		ll.insereInicio(mov3);
		ll.exibeItens();
		System.out.println("-------------------------------------------");
		System.out.println(" Inserindo 3 movimentos no fim ");
		System.out.println("-------------------------------------------");
		ll.insereFim(mov4);
		ll.insereFim(mov5);
		ll.insereFim(mov6);
		ll.exibeItens();
		System.out.println("-------------------------------------------");
		System.out.println(" Insere 3 movimentos no inicio ");
		System.out.println("-------------------------------------------");
		ll.insereInicio(mov7);
		ll.insereInicio(mov8);
		ll.insereInicio(mov9);
		ll.exibeItens();
		System.out.println("-------------------------------------------");
		System.out.println(" Remove 3 movimentos do inicio ");
		System.out.println("-------------------------------------------");
		try {
			ll.removeInicio();
			ll.removeInicio();
			ll.removeInicio();
		} catch (Exception e) {
			System.out.println("Nao Removeu");
		}
		ll.exibeItens();
		*/

	}

}
