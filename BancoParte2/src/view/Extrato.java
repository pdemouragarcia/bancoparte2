package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Extrato extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Extrato frame = new Extrato();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Extrato() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 447, 298);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblExtrato = new JLabel("Extrato");
		lblExtrato.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExtrato.setBounds(195, 11, 60, 28);
		contentPane.add(lblExtrato);
		
		JLabel lblData = new JLabel("Data Inicial :");
		lblData.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblData.setBounds(71, 80, 76, 14);
		contentPane.add(lblData);
		
		textField = new JTextField();
		textField.setBounds(155, 77, 111, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblDataFinal = new JLabel("Data Final:");
		lblDataFinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDataFinal.setBounds(71, 124, 72, 14);
		contentPane.add(lblDataFinal);
		
		textField_1 = new JTextField();
		textField_1.setBounds(155, 121, 111, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Prosseguir");
		btnNewButton.setBounds(54, 203, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(290, 203, 89, 23);
		contentPane.add(btnCancelar);
		
		JButton btnCriarArquivo = new JButton("Criar Arquivo");
		btnCriarArquivo.setBounds(166, 203, 100, 23);
		contentPane.add(btnCriarArquivo);
	}
}
