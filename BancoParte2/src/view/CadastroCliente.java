package view;

/*
 * author @ggirardon
 */

import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DAOBanco;


//import DAO.Banco;



@SuppressWarnings("serial")
public class CadastroCliente extends JFrame implements ActionListener {
	private JLabel label, labelCPF, labelCNPJ, labelRg;
	private JTextField fieldRG, fieldCPF, fieldCNPJ, fieldNome, fieldEndereco, fieldCep, fieldTelefone, fieldRenda;
	private JButton buttonCadastrar, buttonCancelar;
	private JComboBox jComboBoxTipoPessoa;

	// private JRadioButton CheckAtiva, CheckInativa;
	// private ButtonGroup Status;
	@SuppressWarnings({ "rawtypes", "unchecked" })

	public CadastroCliente() {
		super("Cadastrar Novo Cliente"); // titulo do programa
		Container content = getContentPane();
		content.setLayout(null);
		setSize(845, 535);
		setFont(new Font("Arial", Font.BOLD, 12));

		/*
		 * �NICIO DOS CAMPOS TELA DE CADASTRO (x, y, a, b) x = posi��o
		 * horizontal, y = posi��o vertical a = tamanho horizontal, b = tamanho
		 * vertical
		 */
		jComboBoxTipoPessoa = new JComboBox();

		String labelText20 = "Tipo de Pessoa*:";
		label = new JLabel(labelText20);
		label.setBounds(new Rectangle(220, 40, 125, 25));
		label.setFont(new Font("Arial", Font.BOLD, 12));
		content.add(label);
		jComboBoxTipoPessoa.setModel(new DefaultComboBoxModel(new String[] { "", "Fisica", "Juridica" }));
		jComboBoxTipoPessoa.setBounds(340, 40, 200, 25);
		content.add(jComboBoxTipoPessoa);
		jComboBoxTipoPessoa.addActionListener(this);
		
		String labelDoCNPJ = "CNPJ:";
		labelCNPJ = new JLabel(labelDoCNPJ);
		labelCNPJ.setBounds(new Rectangle(220, 70, 125, 25));
		content.add(labelCNPJ);
		labelCNPJ.setVisible(false);
		fieldCNPJ = new JTextField();
		fieldCNPJ.setBounds(new Rectangle(340, 70, 200, 25));
		fieldCNPJ.setVisible(false);
		content.add(fieldCNPJ, null);
		
		String labelDoCpf = "CPF: ";
		labelCPF = new JLabel(labelDoCpf);
		labelCPF.setBounds(new Rectangle(220, 70, 125, 25)); // (x, y, a, b)
		content.add(labelCPF);
		labelCPF.setVisible(false);
		fieldCPF = new JTextField();
		fieldCPF.setBounds(new Rectangle(340, 70, 200, 25));
		fieldCPF.setVisible(false);
		content.add(fieldCPF, null);

		String labelDoRG = "RG: ";
		labelRg = new JLabel(labelDoRG);
		labelRg.setBounds(new Rectangle(220, 110, 125, 25)); // (x, y, a, b)
		content.add(labelRg);
		labelRg.setVisible(false);
		fieldRG = new JTextField();
		fieldRG.setBounds(new Rectangle(340, 110, 200, 25));
		fieldRG.setVisible(false);
		content.add(fieldRG, null);

		String labelDoNome = "Nome Completo: ";
		label = new JLabel(labelDoNome);
		label.setBounds(new Rectangle(220, 150, 125, 25));
		content.add(label);
		fieldNome = new JTextField();
		fieldNome.setBounds(new Rectangle(340, 150, 200, 25));
		content.add(fieldNome, null);

		String labelDoEndereco = "Endere�o: ";
		label = new JLabel(labelDoEndereco);
		label.setBounds(new Rectangle(220, 190, 125, 25));
		content.add(label);
		fieldEndereco = new JTextField();
		fieldEndereco.setBounds(new Rectangle(340, 190, 200, 25));
		content.add(fieldEndereco, null);

		String labelDoCep = "CEP: ";
		label = new JLabel(labelDoCep);
		label.setBounds(new Rectangle(220, 230, 125, 25));
		content.add(label);
		fieldCep = new JTextField();
		fieldCep.setBounds(new Rectangle(340, 230, 200, 25));
		content.add(fieldCep, null);

		String labelDoTelefone = "Telefone: ";
		label = new JLabel(labelDoTelefone);
		label.setBounds(new Rectangle(220, 270, 125, 25));
		content.add(label);
		fieldTelefone = new JTextField();
		fieldTelefone.setBounds(new Rectangle(340, 270, 200, 25));
		content.add(fieldTelefone, null);

		String labelDaRenda = "Renda: ";
		label = new JLabel(labelDaRenda);
		label.setBounds(new Rectangle(220, 310, 125, 25));
		content.add(label);
		fieldRenda = new JTextField();
		fieldRenda.setBounds(new Rectangle(340, 310, 200, 25));
		content.add(fieldRenda, null);

			/*
		 * FIM DOS CAMPOS TELA DE CADASTRO
		 */

		/*
		 * �NICIO BOT�ES CADASTRAR E CANCELAR
		 */

		buttonCadastrar = new JButton("SALVAR CADASTRO");
		buttonCadastrar.setBounds(new Rectangle(230, 440, 200, 35));
		content.add(buttonCadastrar, null);
		buttonCadastrar.setActionCommand("OK");
		buttonCadastrar.addActionListener(this);


		buttonCancelar = new JButton("CANCELAR");
		buttonCancelar.setBounds(new Rectangle(480, 440, 200, 35));
		content.add(buttonCancelar, null);
		buttonCancelar.setActionCommand("CANCELAR");
		buttonCancelar.addActionListener(this);
		

		setVisible(true);
		setResizable(false); // N�o deixa a tela ser redimensionada

		/*
		 * FIM BOT�ES CADASTRAR E CANCELAR
		 */
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
		fieldCPF.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 1);
		labelCPF.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 1);
		fieldRG.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 1);
		labelRg.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 1);

		fieldCNPJ.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 2);
		labelCNPJ.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 2);
		
		if (comando.equals("CANCELAR")) { // fun��o do bot�o cancelar
			new Principal();
			this.dispose();
		}

		if (comando.equals("OK") && jComboBoxTipoPessoa.getSelectedIndex() == 1) { // fun��o do bot�o ok
			String nome = fieldNome.getText();
			String endereco = fieldEndereco.getText();
			String cep = fieldCep.getText();
			String telefone = fieldTelefone.getText();
            String renda = fieldRenda.getText();
			String cpf = fieldCPF.getText();
			String rg = fieldRG.getText();
			
			DAOBanco.converteEcadastraFisico(nome, endereco, cep, telefone, renda, cpf, rg);
			}
		
		if (comando.equals("OK") && jComboBoxTipoPessoa.getSelectedIndex() == 2) { // fun��o do bot�o ok
			String nome = fieldNome.getText();
			String endereco = fieldEndereco.getText();
			String cep = fieldCep.getText();
			String telefone = fieldTelefone.getText();
            String renda = fieldRenda.getText();
			String cnpj = fieldCNPJ.getText();
			
			DAOBanco.converteEcadastraJuridico(nome, endereco, cep, telefone, renda, cnpj);

		}
	
	}
}
