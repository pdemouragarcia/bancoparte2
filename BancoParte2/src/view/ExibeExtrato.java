package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class ExibeExtrato extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ExibeExtrato frame = new ExibeExtrato();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ExibeExtrato() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 314);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblExtrato = new JLabel("Extrato");
		lblExtrato.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExtrato.setBounds(190, 0, 46, 14);
		panel.add(lblExtrato);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 24, 404, 200);
		panel.add(textArea);
		
		JButton btnOk = new JButton("Ok");
		btnOk.setBounds(170, 235, 89, 23);
		panel.add(btnOk);
	}
}
