package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class AcessaConta extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField fieldNmroConta;
	private JTextField fieldSenha;

	public AcessaConta() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAcessarConta = new JLabel("Acessar Conta");
		lblAcessarConta.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 16));
		lblAcessarConta.setBounds(174, 29, 85, 14);
		contentPane.add(lblAcessarConta);
		
		JLabel lblTipoDeConta = new JLabel("Tipo de conta:");
		lblTipoDeConta.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTipoDeConta.setBounds(68, 78, 94, 14);
		contentPane.add(lblTipoDeConta);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Conta Corrente", "Conta Especial", "Conta Poupan\u00E7a"}));
		comboBox.setBounds(174, 72, 118, 20);
		contentPane.add(comboBox);
		
		JLabel lblNewLabel = new JLabel("N\u00FAmero da conta:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(68, 110, 94, 14);
		contentPane.add(lblNewLabel);
		
		fieldNmroConta = new JTextField();
		fieldNmroConta.setBounds(173, 107, 119, 20);
		contentPane.add(fieldNmroConta);
		fieldNmroConta.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSenha.setBounds(68, 144, 94, 14);
		contentPane.add(lblSenha);
		
		fieldSenha = new JTextField();
		fieldSenha.setColumns(10);
		fieldSenha.setBounds(173, 141, 119, 20);
		contentPane.add(fieldSenha);
		
		JButton Acessar = new JButton("Acessar");
		Acessar.setBounds(110, 200, 89, 23);
		Acessar.setActionCommand("OK");
		Acessar.addActionListener(this);
		contentPane.add(Acessar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(235, 200, 89, 23);
		contentPane.add(btnSair);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();

		if (comando.equals("EXIT")) {
			new Principal();
			this.dispose();
		}
		if (comando.equals("OK")) {
			int nmroConta = Integer.parseInt(fieldNmroConta.getText());
			int senha = Integer.parseInt(fieldSenha.getText());
			//System.out.println("JANELA: " + nmroConta + " " + senha);
			//DAOBanco.imprimeContasCorrentes();
			//if (DAOBanco.acessaContaCorrente(nmroConta, senha) == true) {
				//new JanelaMenuSaqueDepositoCC(nmroConta);
				//this.dispose();
		//	} else {
			//	JOptionPane.showMessageDialog(null, "Senha Incorreta", "Erro", 1, null);
			}
		}
		
	}

