package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Saque extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	
	public Saque() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSaldo = new JLabel("Saldo:");
		lblSaldo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSaldo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSaldo.setBounds(81, 28, 110, 14);
		contentPane.add(lblSaldo);
		
		JLabel lblNmeroDaConta = new JLabel("N\u00FAmero da conta:");
		lblNmeroDaConta.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNmeroDaConta.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNmeroDaConta.setBounds(81, 53, 110, 14);
		contentPane.add(lblNmeroDaConta);
		
		JLabel labelRetornaSaldo = new JLabel("0.0");
		labelRetornaSaldo.setBounds(218, 29, 46, 14);
		contentPane.add(labelRetornaSaldo);
		
		JLabel labelRetornaConta = new JLabel("0");
		labelRetornaConta.setBounds(218, 54, 46, 14);
		contentPane.add(labelRetornaConta);
		
		JLabel lblNewLabel = new JLabel("Valor:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(81, 122, 110, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(208, 116, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnSacar = new JButton("Sacar");
		btnSacar.setBounds(208, 147, 89, 23);
		contentPane.add(btnSacar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(208, 215, 89, 23);
		contentPane.add(btnSair);
		
		JLabel lblNewLabel_1 = new JLabel("Limite:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(81, 82, 110, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel labelRetornaLimite = new JLabel("0");
		labelRetornaLimite.setBounds(218, 83, 46, 14);
		contentPane.add(labelRetornaLimite);
		
		JButton btnDepositar = new JButton("Depositar");
		btnDepositar.setBounds(208, 181, 89, 23);
		contentPane.add(btnDepositar);
	}
}
