package view;

/*
 * author @ggirardon
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import img.*;
import dao.DAOCliente;
import dao.DAOConta;
import model.ClienteFisico;
import model.ContaCorrente;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

@SuppressWarnings("serial")
public class ListarDeletarContaCorrente extends JFrame implements ActionListener {
	JPanel painelFundo;
	JTable table;
	JScrollPane barraRolagem;
	private JButton buttonVoltar, buttonRemover, buttonEditar;
	public int idpassada;
	boolean variavel;
	DefaultTableModel tableModel;

	public ListarDeletarContaCorrente() {
		super("Contas Correntes Cadastrados no Sistema");
		setResizable(false);
		Container content = getContentPane();
		content.setLayout(null);

		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(1, 1));
		painelFundo.setBounds(50, 100, 1150, 450);

		String[] col = { "N�mero da Conta", "Data de Abertura", "Data de Encerramento", "Saldo Atual", "Identifica��o" };
		tableModel = new DefaultTableModel(col, 0) {
			public boolean isCellEditable(int rowIndex, int mColIndex) {
				return false;
			}
		};
		table = new JTable(tableModel);

		ArrayList<ContaCorrente> contasCorrentes = (ArrayList<ContaCorrente>) DAOConta.getContasCorrentes();
		for (int i = 0; i < contasCorrentes.size(); i++) {

			int NumeroConta = contasCorrentes.get(i).getNumeroConta();
			Date DataAbertura = contasCorrentes.get(i).getDataAbertura();
			Date DataEncerramento = contasCorrentes.get(i).getDataEncerramento();
			double Saldo = contasCorrentes.get(i).getSaldo();
			String Identi = contasCorrentes.get(i).getIdentificacao();
			// Date DataCadastro = clientesFisicos.get(i).getDataCadastro();

			Object[] data = { NumeroConta, DataAbertura, DataEncerramento, Saldo, Identi};
			tableModel.addRow(data);
		}

		barraRolagem = new JScrollPane(table);
		painelFundo.add(barraRolagem);
		getContentPane().add(painelFundo);

		buttonRemover = new JButton("Remover Conta");
		buttonRemover.setBounds(new Rectangle(200, 610, 200, 40));
		content.add(buttonRemover, null);
		buttonRemover.setActionCommand("REMOVER");
		buttonRemover.addActionListener(this);

		buttonVoltar = new JButton("Voltar");
		buttonVoltar.setBounds(new Rectangle(500, 610, 200, 40));
		content.add(buttonVoltar, null);
		buttonVoltar.setActionCommand("EXIT");
		buttonVoltar.addActionListener(this);

		buttonEditar = new JButton("Editar Cliente");
		buttonEditar.setBounds(new Rectangle(800, 610, 200, 40));
		content.add(buttonEditar, null);
		buttonEditar.setActionCommand("EDITAR");
		buttonEditar.addActionListener(this);

		setSize(1300, 700);
		setVisible(true);
		
		
		}
	
	
	public void verificaRemoveLinha() {
		int x;
		x = table.getSelectedRow();
		int y = (int) table.getValueAt(x, 0);
		int numero = (int) table.getValueAt(x, 0);
		String identificacao = (String) table.getValueAt(x, 4);
		String message = "Deseja deletar a conta: " + numero;
		String title = "Excluindo conta";
		int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			//if (identificacao == null && removeDoArrayContaCorrente(y) == true) { // deletar apenas se o campo identificacao (cpf) for vazio
			if (removeDoArrayContaCorrente(y) == true) {
				JOptionPane.showMessageDialog(null, " CADASTRO DELETADO ", "", JOptionPane.INFORMATION_MESSAGE);
				this.dispose();
				new ListarDeletarContaCorrente();
			}
		}
	}

	public boolean removeDoArrayContaCorrente(int a) {
		ArrayList<ContaCorrente> contasCorrentes = (ArrayList<ContaCorrente>) DAOConta.getContasCorrentes();
		for (int c = 0; c <= contasCorrentes.size(); c++) {
			if (contasCorrentes.get(c).getNumeroConta() == a) {
				contasCorrentes.remove(c);
				DAOConta.escreveVetorContaCorrente();
				return true;
			}
		}
		return false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();

		if (comando.equals("EXIT")) {
			new Principal();
			this.dispose();
		}
		if (comando.equals("REMOVER")) {
			if (table.getSelectedRows().length == 0) {
				JOptionPane.showMessageDialog(null, " INFORME UM CLIENTE ", "", JOptionPane.ERROR_MESSAGE);
			} else if (table.getSelectedRowCount() == 1) {
				verificaRemoveLinha();
			} else {
				JOptionPane.showMessageDialog(null, " SELECIONE UM CLIENTE APENAS ", "", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
