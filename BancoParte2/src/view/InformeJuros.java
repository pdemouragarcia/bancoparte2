package view;

import java.awt.Container;

/*
 * author @ggirardon
 */

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import dao.DAOBanco;

import javax.swing.JButton;

//import DAO.Banco;

@SuppressWarnings("serial")
public class InformeJuros extends JFrame implements ActionListener {
	private JTextField textJuros;

	// private JRadioButton CheckAtiva, CheckInativa;
	// private ButtonGroup Status;
	@SuppressWarnings({})

	public InformeJuros() {
		super("Cadastrar Novo Cliente"); // titulo do programa
		setTitle("Sistema Banc�rio - Menu Principal");
		Container content = getContentPane();
		content.setLayout(null);
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 819, 485);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblInformeJurosDo = new JLabel("informe juros do dia");
		lblInformeJurosDo.setBounds(186, 80, 152, 14);
		panel.add(lblInformeJurosDo);

		textJuros = new JTextField();
		textJuros.setBounds(208, 117, 86, 20);
		panel.add(textJuros);
		textJuros.setColumns(10);

		JButton btnCalcularJuros = new JButton("calcular juros");
		btnCalcularJuros.setBounds(229, 210, 152, 23);
		content.add(btnCalcularJuros, null);
		panel.add(btnCalcularJuros);
		btnCalcularJuros.setActionCommand("OK");
		btnCalcularJuros.addActionListener(this);

		setSize(845, 535);
		setFont(new Font("Arial", Font.BOLD, 12));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false); // N�o deixa a tela ser redimensionada

		// JMENUBAR TOPO DA TELA
		
		setJMenuBar(createMenuBar());
		setVisible(true);
	}
	
		public JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnPrincipal = new JMenu("Principal");
		JMenuItem mntmnicio = new JMenuItem("�nicio");

		JMenu mnCliente = new JMenu("Clientes");
		JMenuItem mntmCadastrarCliente = new JMenuItem("Cadastrar Cliente");
		JMenuItem mntmConsultarCliente = new JMenuItem("Consultar Cliente F�sico");
		JMenuItem mntmConsultarClienteJurdico = new JMenuItem("Consultar Cliente Jur�dico");

		JMenu mnContas = new JMenu("Contas");
		JMenuItem mntmCadastrarConta = new JMenuItem("Cadastrar Conta");
		JMenuItem mntmListarContaCorrente = new JMenuItem("Listar Contas Correntes");
		JMenuItem mntmListarContasEspeciais = new JMenuItem("Listar Contas Especiais");
		JMenuItem mntmListarContasPoupana = new JMenuItem("Listar Contas Poupan�a");

		JMenu mnRealizarOperaes = new JMenu("Realizar Opera��es");
		JMenuItem mntmRealizarDepsito = new JMenuItem("Realizar Dep�sito");
		JMenuItem mntmRealizarSaque = new JMenuItem("Realizar Saque");

		mntmnicio.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new InformeJuros();
				dispose();
			}
		});

		mntmCadastrarCliente.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new CadastroCliente();
				dispose();
			}
		});

		mntmConsultarCliente.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new ListarDeletarClienteFisico();
				dispose();
			}
		});

		mntmConsultarClienteJurdico.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new ListarDeletarClienteJuridico();
				dispose();
			}
		});

		mntmCadastrarConta.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new CadastroConta();
				dispose();
			}
		});

		mntmListarContaCorrente.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new ListarDeletarContaCorrente();
				dispose();
			}
		});

		mntmListarContasEspeciais.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new ListarDeletarContaEspecial();
				dispose();
			}
		});

		mntmListarContasPoupana.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				new ListarDeletarContaPoupanca();
				dispose();
			}
		});

		menuBar.add(mnPrincipal);
		menuBar.add(mnCliente);
		menuBar.add(mnContas);
		menuBar.add(mnRealizarOperaes);

		mnPrincipal.add(mntmnicio);
		mnCliente.add(mntmCadastrarCliente);
		JSeparator separator_1 = new JSeparator();
		mnCliente.add(separator_1);
		mnCliente.add(mntmConsultarCliente);
		JSeparator separator_2 = new JSeparator();
		mnCliente.add(separator_2);

		mnCliente.add(mntmConsultarClienteJurdico);
		mnContas.add(mntmCadastrarConta);
		JSeparator separator_4 = new JSeparator();
		mnContas.add(separator_4);
		mnContas.add(mntmListarContaCorrente);

		JSeparator separator_7 = new JSeparator();
		mnContas.add(separator_7);

		mnContas.add(mntmListarContasEspeciais);

		JSeparator separator_8 = new JSeparator();
		mnContas.add(separator_8);

		mnContas.add(mntmListarContasPoupana); // here
		mnRealizarOperaes.add(mntmRealizarDepsito);
		JSeparator separator_6 = new JSeparator();
		mnRealizarOperaes.add(separator_6);
		mnRealizarOperaes.add(mntmRealizarSaque);

		JSeparator separator_5 = new JSeparator();
		menuBar.add(separator_5);
		JSeparator separator_3 = new JSeparator();
		menuBar.add(separator_3);
		JSeparator separator = new JSeparator();
		menuBar.add(separator);
		
		return menuBar;
	}
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();

		if (comando.equals("CANCELAR")) { // fun��o do bot�o cancelar
			// new ViewMenuOperador();
			// this.dispose();
		}

		if (comando.equals("OK")) { // fun��o do bot�o ok
			String juros = textJuros.getText();
			
			DAOBanco.calculaEGravaJurosPoupanca(juros);
			
			

		}

	}
}