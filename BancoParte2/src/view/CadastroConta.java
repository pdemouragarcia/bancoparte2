package view;
/*
 * author @ggirardon
 */

import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import dao.DAOBanco;
import javax.swing.SwingConstants;


@SuppressWarnings("serial")
public class CadastroConta extends JFrame implements ActionListener {
	// private JLabel label;
	// private JTextField fieldNumeroConta, fieldDataAbertura,
	// fieldDataEncerramento, fieldSituacao, fieldSenha, fieldSaldo;
	private JButton buttonCadastrar, buttonCancelar;
	private JTextField fieldIdentifica;
	private JTextField fieldSenha;
	private JTextField fieldSaldo;
	private JComboBox comboBox;
	private JLabel lblTipoConta;
	private JComboBox comboBoxTipoConta;
	private JLabel lblLimite;
	private JTextField fieldLimite;
	// private JRadioButton tipoContaCorrente, tipoContaCorrentePoupanca,
	// tipoContaCorrenteEspecial;

	public CadastroConta() {
		super("Cadastrar Nova Conta"); // titulo do programa
		Container content = getContentPane();
		content.setLayout(null);
		setSize(845, 535);
		setFont(new Font("Arial", Font.BOLD, 12));

		buttonCadastrar = new JButton("SALVAR CADASTRO");
		buttonCadastrar.setBounds(new Rectangle(230, 440, 200, 35));
		content.add(buttonCadastrar, null);
		buttonCadastrar.setActionCommand("OK");
		buttonCadastrar.addActionListener(this);


		buttonCancelar = new JButton("CANCELAR");
		buttonCancelar.setBounds(new Rectangle(480, 440, 200, 35));
		content.add(buttonCancelar, null);
		buttonCancelar.setActionCommand("CANCELAR");
		buttonCancelar.addActionListener(this);


		comboBox = new JComboBox();
		comboBox.setBounds(430, 171, 143, 20);
		getContentPane().add(comboBox);
		comboBox.addItem("");
		comboBox.addItem("F�sico");
		comboBox.addItem("Jur�dico");

		JLabel lblTipoDoCliente = new JLabel("Tipo do cliente:");
		lblTipoDoCliente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipoDoCliente.setBounds(324, 171, 105, 17);
		getContentPane().add(lblTipoDoCliente);

		JLabel lblCpfcnpj = new JLabel("CPF/CNPJ:");
		lblCpfcnpj.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCpfcnpj.setBounds(324, 214, 96, 14);
		getContentPane().add(lblCpfcnpj);

		fieldIdentifica = new JTextField();
		fieldIdentifica.setBounds(430, 213, 143, 20);
		getContentPane().add(fieldIdentifica);
		fieldIdentifica.setColumns(10);

		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSenha.setBounds(324, 302, 46, 14);
		getContentPane().add(lblSenha);

		fieldSenha = new JTextField();
		fieldSenha.setColumns(10);
		fieldSenha.setBounds(430, 301, 143, 20);
		getContentPane().add(fieldSenha);

		JLabel lblSaldoInicial = new JLabel("Saldo Inicial:");
		lblSaldoInicial.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSaldoInicial.setBounds(325, 344, 83, 14);
		getContentPane().add(lblSaldoInicial);

		fieldSaldo = new JTextField();
		fieldSaldo.setColumns(10);
		fieldSaldo.setBounds(430, 338, 143, 20);
		getContentPane().add(fieldSaldo);

		JLabel lblNovaContaCorrente = new JLabel("Cadastrar Conta Corrente");
		lblNovaContaCorrente.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNovaContaCorrente.setBounds(337, 58, 216, 20);
		getContentPane().add(lblNovaContaCorrente);
		
		lblTipoConta = new JLabel("Tipo da Conta");
		lblTipoConta.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipoConta.setBounds(324, 129, 105, 17);
		getContentPane().add(lblTipoConta);
		
		comboBoxTipoConta = new JComboBox();
		comboBoxTipoConta.setModel(new DefaultComboBoxModel(new String[] {"", "Conta Corrente", "Conta Especial", "Conta Poupan\u00E7a"}));
		comboBoxTipoConta.setBounds(430, 129, 143, 20);
		getContentPane().add(comboBoxTipoConta);
		comboBoxTipoConta.addActionListener(this);
		
		lblLimite = new JLabel("Limite:");
		lblLimite.setHorizontalAlignment(SwingConstants.LEFT);
		lblLimite.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLimite.setBounds(324, 261, 96, 14);
		getContentPane().add(lblLimite);
		lblLimite.setVisible(false);
		
		
		fieldLimite = new JTextField();
		fieldLimite.setColumns(10);
		fieldLimite.setBounds(430, 260, 143, 20);
		getContentPane().add(fieldLimite);
		fieldLimite.setVisible(false);
		content.add(fieldLimite);
		

		setVisible(true);
		setResizable(false); // N�o deixa a tela ser redimensionada

		/*
		 * FIM BOT�ES CADASTRAR E CANCELAR
		 */
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
		fieldLimite.setVisible(comboBoxTipoConta.getSelectedIndex() == 2);
		lblLimite.setVisible(comboBoxTipoConta.getSelectedIndex() == 2);
		
		if (comando.equals("CANCELAR")) { // fun��o do bot�o cancelar
			new Principal();
			this.dispose();
			
		}
		if (comando.equals("OK")) {

			// PROCURA O CLIENTE NO ARRAY CLIENTE F�SICO, SE EXISTIR, SALVA
			// CONTA
			String identificacao = fieldIdentifica.getText();
						String senha = fieldSenha.getText();
						String saldo = fieldSaldo.getText();
						String limite = fieldLimite.getText();
						String cliente = "";
						if (comboBox.getSelectedIndex() == 1) {
							cliente = "fisico";

			 			}
						if (comboBox.getSelectedIndex() == 2) {
							cliente = "juridico";
						}
						
						if (comboBoxTipoConta.getSelectedIndex() == 1 && DAOBanco.converteEcadastraCC(identificacao, senha, saldo, cliente) == true) {
							JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso");
							
						} else if (comboBoxTipoConta.getSelectedIndex() == 2 && DAOBanco.converteEcadastraCE(identificacao, senha, saldo, limite, cliente) == true) {
							JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso");
						
						} else if (comboBoxTipoConta.getSelectedIndex() == 3 && DAOBanco.converteEcadastraCP(identificacao, senha, saldo, cliente) == true) {
							JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso");
						} else {
							JOptionPane.showMessageDialog(null, "Cliente n�o cadastrado");
						}

		}

	}
}
