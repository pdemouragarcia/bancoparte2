package view;

/*
 * author @ggirardon
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import img.*;
import dao.DAOCliente;
import model.ClienteFisico;
import model.ClienteJuridico;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

@SuppressWarnings("serial")
public class ListarDeletarClienteJuridico extends JFrame implements ActionListener {
	JPanel painelFundo;
	JTable table;
	JScrollPane barraRolagem;
	private JButton buttonVoltar, buttonRemover, buttonEditar;
	public int idpassada;
	boolean variavel;
	DefaultTableModel tableModel;

	public ListarDeletarClienteJuridico() {
		super("Clientes Jur�dicos Cadastrados no Sistema");
		setResizable(false);
		Container content = getContentPane();
		content.setLayout(null);

		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(1, 1));
		painelFundo.setBounds(50, 100, 1150, 450);

		String[] col = { "Nome", "CNPJ", "CEP", "Endere�o", "Telefone", "Renda" };
		tableModel = new DefaultTableModel(col, 0) {
			public boolean isCellEditable(int rowIndex, int mColIndex) {
				return false;
			}
		};
		table = new JTable(tableModel);

		ArrayList<ClienteJuridico> clientesJuridicos = (ArrayList<ClienteJuridico>) DAOCliente.getClientesJuridicos();
		for (int i = 0; i < clientesJuridicos.size(); i++) {

			String Nome = clientesJuridicos.get(i).getNome();
			int Cnpj = clientesJuridicos.get(i).getCnpj();
			String Endereco = clientesJuridicos.get(i).getEndereco();
			String Cep = clientesJuridicos.get(i).getCep();
			int Telefone = clientesJuridicos.get(i).getTelefone();
			double Renda = clientesJuridicos.get(i).getRenda();
			//Date DataCadastro = clientesJuridicos.get(i).getDataCadastro();

			Object[] data = { Nome, Cnpj, Cep, Endereco, Telefone, Renda };
			tableModel.addRow(data);
		}

		barraRolagem = new JScrollPane(table);
		painelFundo.add(barraRolagem);
		getContentPane().add(painelFundo);

		buttonRemover = new JButton("Remover Cliente");
		buttonRemover.setBounds(new Rectangle(200, 610, 200, 40));
		content.add(buttonRemover, null);
		buttonRemover.setActionCommand("REMOVER");
		buttonRemover.addActionListener(this);

		buttonVoltar = new JButton("Voltar");
		buttonVoltar.setBounds(new Rectangle(500, 610, 200, 40));
		content.add(buttonVoltar, null);
		buttonVoltar.setActionCommand("EXIT");
		buttonVoltar.addActionListener(this);

		buttonEditar = new JButton("Editar Cliente");
		buttonEditar.setBounds(new Rectangle(800, 610, 200, 40));
		content.add(buttonEditar, null);
		buttonEditar.setActionCommand("EDITAR");
		buttonEditar.addActionListener(this);

		setSize(1300, 700);
		setVisible(true);
		
		
		}
	
	public void verificaRemoveLinha() {
		int x;
		x = table.getSelectedRow();
		int y = (int) table.getValueAt(x, 1);
		String cli = (String) table.getValueAt(x, 0);
		String message = "Deseja deletar o cliente: " + cli + "\nCNPJ: " + y;
		String title = "Excluindo cliente";
		int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			if (removeDoArrayClienteFisico(y) == true) {
				JOptionPane.showMessageDialog(null, " CADASTRO DELETADO ", "", JOptionPane.INFORMATION_MESSAGE);
				this.dispose();
				new ListarDeletarClienteJuridico();
			}
		}
	}

	public boolean removeDoArrayClienteFisico(int a) {
		ArrayList<ClienteJuridico> clientesJuridicos = (ArrayList<ClienteJuridico>) DAOCliente.getClientesJuridicos();
		for (int c = 0; c <= clientesJuridicos.size(); c++) {
			if (clientesJuridicos.get(c).getCnpj() == a) {
				clientesJuridicos.remove(c);
				DAOCliente.escreveVetorClienteJuridico();
				return true;
			}
		}
		return false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();

		if (comando.equals("EXIT")) {
			new Principal();
			this.dispose();
		}
		if (comando.equals("REMOVER")) {
			if (table.getSelectedRows().length == 0) {
				JOptionPane.showMessageDialog(null, " INFORME UM CLIENTE ", "", JOptionPane.ERROR_MESSAGE);
			} else if (table.getSelectedRowCount() == 1) {
				verificaRemoveLinha();
			} else {
				JOptionPane.showMessageDialog(null, " SELECIONE UM CLIENTE APENAS ", "", JOptionPane.ERROR_MESSAGE);
			}
		}
		if (comando.equals("EDITAR")) {
			if (table.getSelectedRowCount() == 0) {
				JOptionPane.showMessageDialog(null, " SELECIONE UM CLIENTE ", "", JOptionPane.ERROR_MESSAGE);
			} else if (table.getSelectedRowCount() == 1) {
				int x;
				x = table.getSelectedRow();
				int y = (int) table.getValueAt(x, 0);
				// new JanelaEditar(y);
				// this.dispose();
			} else {
				JOptionPane.showMessageDialog(null, " Selecione Apenas Um Cliente ", "",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}
