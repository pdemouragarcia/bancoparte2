package model;

public class ClienteFisico extends Cliente {

	private int Cpf;
	private int Rg;

	public int getCpf() {
		return Cpf;
	}

	public void setCpf(int cpf) {
		Cpf = cpf;
	}

	public int getRg() {
		return Rg;
	}

	public void setRg(int rg) {
		Rg = rg;
	}

	public ClienteFisico(String nome, String endereco, String cep, int telefone, double renda, int cpf, int rg) {
		super(nome, endereco, cep, telefone, renda);
		this.Cpf = cpf;
		this.Rg = rg;
	}

}
