package model;

public class ClienteJuridico extends Cliente {
	private int Cnpj;

	public ClienteJuridico(String nome, String endereco, String cep, int telefone, double renda, int CNPJ) {
		super(nome, endereco, cep, telefone, renda);
		this.Cnpj = CNPJ;
	}

	public int getCnpj() {
		return Cnpj;
	}

	public void setCnpj(int cnpj) {
		Cnpj = cnpj;
	}

}
