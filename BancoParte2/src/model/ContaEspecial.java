package model;

import java.io.Serializable;
import java.util.Date;

public class ContaEspecial extends ContaCorrente {

	double limite;

	public ContaEspecial(int nmro, boolean situacao, double saldo, Date dataAbertura, int senha, String identificacao,
			double limite) {
		super(nmro, situacao, saldo, dataAbertura, senha, identificacao);
		this.limite = limite;
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}

	@Override
	public boolean saca(double valor, int senha) {
		if ((validaSenha(senha) == true) && (valor <= (this.saldo + this.limite))) {
			this.saldo = this.saldo - valor;
			// System.out.print("Saque efetuado com sucesso");
			return true;
		} else {
			// System.out.print("Saldo indisponível");
			return false;
		}
	}

	public boolean alteraLimite(double valor, int senha) {
		if (validaSenha(senha) == true) {
			this.setLimite(valor);
			return true;
		} else {
			return false;

		}

	}
}
