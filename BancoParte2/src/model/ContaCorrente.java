package model;

import java.io.Serializable;
import java.util.Date;

import dao.ListaLigada;
import dao.No;

public class ContaCorrente implements Serializable {
	int numeroConta;
	Date dataAbertura;
	Date dataEncerramento;
	boolean aberta;
	int senha;
	String identificacao;
	double saldo;
	ListaLigada movimentos;

	public ContaCorrente(int nmro, boolean situacao, double saldo,
			Date dataAbertura, int senha, String identificacao) {
		this.numeroConta = nmro;
		this.aberta = situacao;
		this.saldo = saldo;
		this.senha = senha;
		this.dataAbertura = new Date();
		this.identificacao = identificacao;
		this.dataEncerramento = null;
		this.movimentos = new ListaLigada();

	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public int getNumeroConta() {
		return numeroConta;
	}

	private void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	private void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	private void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public boolean isAberta() {
		return aberta;
	}

	private void setAberta(boolean situacao) {
		this.aberta = situacao;
	}

	public int getSenha() {
		return senha;
	}

	private void setSenha(int senha) {
		this.senha = senha;
	}

	public double getSaldo() {
		return saldo;
	}

	private void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public boolean saca(double valor, int senha) {
		if ((validaSenha(senha) == true) && (valorPositivo(valor) == true)
				&& (temSaldo(valor) == true)) {
			double novoSaldo;
			novoSaldo = (this.getSaldo() - valor);
			setSaldo(novoSaldo);
			return true;
		} else {
			return false;
		}
	}

	private boolean temSaldo(double valor) {
		if ((this.getSaldo() - valor) < 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean valorPositivo(double valor) {
		if (valor >= 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean deposita(double valor, int senha) {
		if ((validaSenha(senha) == true) && (valorPositivo(valor) == true)) {
			double novoSaldo;
			novoSaldo = (this.getSaldo() + valor);
			setSaldo(novoSaldo);
			return true;
		} else {
			return false;
		}
	}

	public boolean encerraConta(int senha) {
		if (validaSenha(senha) == true) {
			setAberta(false);
			return true;
		}
		return false;
	}

	public boolean validaSenha(int s) {
		if (s == this.senha) {
			return true;
		} else {
			return false;
		}
	}

	public ListaLigada getMovimentos() {
		return movimentos;
	}

	public void setMovimentos(ListaLigada extrato) {
		this.movimentos = extrato;
	}
/*
 * M�TODO QUE BUSCA OS MOVIMENTOS DE UMA DETERMINADA CONTA
 * EM UM DETERMINADO INTERVALO USANDO UM M�TODO toString da classe movimento E RETORNA
 * UMA STRING ESTRUTURADA COM ELES.
 */
	public String montaExtrato(Date inicio, Date fim) {
		String s = "";
		No aux = movimentos.getNo();
		if (!movimentos.ehVazia()) {
			while (aux.getProx() != null || aux != null) {
				if (aux.getMovimento().getDataMovimento().after(inicio)
						|| aux.getMovimento().getDataMovimento()
								.equals(inicio)
						&& aux.getMovimento().getDataMovimento()
								.before(fim)
						|| aux.getMovimento().getDataMovimento()
								.equals(fim)) {
					s += aux.toString();
				}

				aux = aux.getProx();
			}
		}
		return s;
	}
}