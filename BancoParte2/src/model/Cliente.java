package model;

import java.io.Serializable;

abstract class Cliente implements Serializable {
	protected String nome;
	protected String endereco;
	protected String cep;
	protected int telefone;
	protected double renda;

	public Cliente(String nome, String endereco, String cep, int telefone2, double renda) {

		this.nome = nome;
		this.endereco = endereco;
		this.cep = cep;
		this.telefone = telefone2;
		this.renda = renda;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public int getTelefone() {
		return telefone;
	}

	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}

	public double getRenda() {
		return renda;
	}

	public void setRenda(double renda) {
		this.renda = renda;
	}
	
}  


