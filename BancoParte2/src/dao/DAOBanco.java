package dao;

import java.util.Date;
import java.util.List;
import java.util.Random;

import model.ContaCorrente;
import model.ContaEspecial;
import model.ContaPoupanca;
import model.ClienteFisico;
import model.ClienteJuridico;

public class DAOBanco {
	static DAOCliente dc = new DAOCliente();
	static DAOConta dco = new DAOConta();

	public void carregaArrays() {
		dc.recuperaVetorClienteFisico();
		dc.recuperaVetorClienteJuridico();
		dco.recuperaVetorContaCorrente();
		dco.recuperaVetorContaEspecial();
		dco.recuperaVetorContaPoupanca();
	}

	public static List<ClienteFisico> retornaArrayClientesFisicos() {
		return dc.getClientesFisicos();
	}

	public static List<ClienteJuridico> retornaArrayClientesJuridicos() {
		return dc.getClientesJuridicos();
	}

	public static List<ContaCorrente> retornaArrayContasCorrentes() {
		return dco.getContasCorrentes();
	}

	public static List<ContaEspecial> retornaArrayContasEspecias() {
		return dco.getContasEspeciais();
	}

	public static List<ContaPoupanca> retornaArrayContasPoupancas() {
		return dco.getContasPoupanca();
	}

	// INICIO METODOS CLIENTE FISICO
	public static void cadastrarClienteFisico(String nome, String endereco,
			String cep, int telefone2, double renda2, int cpf2, int rg2) {
		dc.cadastraCliente(nome, endereco, cep, telefone2, renda2, cpf2, rg2);

	}

	public static void converteEcadastraFisico(String nome, String endereco,
			String cep, String telefone, String renda, String cpf, String rg) {
		double renda2;
		renda2 = Double.parseDouble(renda);
		int cpf2, telefone2, rg2;
		cpf2 = Integer.parseInt(cpf);
		telefone2 = Integer.parseInt(telefone);
		rg2 = Integer.parseInt(rg);
		cadastrarClienteFisico(nome, endereco, cep, telefone2, renda2, cpf2,
				rg2);

	}

	public void removeCFisico(int cpf) {
		dc.removeClienteFisico(cpf);
	}

	public void converteEalteraFisico(String nome, String endereco, String cep,
			String telefone, String renda, String cpf, String rg) {
		double renda2;
		renda2 = Double.parseDouble(renda);
		int cpf2, telefone2, rg2;
		cpf2 = Integer.parseInt(cpf);
		telefone2 = Integer.parseInt(telefone);
		rg2 = Integer.parseInt(rg);
		dc.atualizaClienteFisico(cpf2, dc.retornaCliente(nome, endereco, cep,
				telefone2, renda2, cpf2, rg2));
	}

	public static Object mandaArrayCFparaView() {
		for (int i = 0; i < dc.getClientesFisicos().size(); i++) {
			String Nome = DAOBanco.retornaArrayClientesFisicos().get(i)
					.getNome();
			String CPF = String.valueOf(DAOBanco.retornaArrayClientesFisicos()
					.get(i).getCpf());
			String RG = String.valueOf(DAOBanco.retornaArrayClientesFisicos()
					.get(i).getRg());
			String Renda = String.valueOf(DAOBanco
					.retornaArrayClientesFisicos().get(i).getRenda());
			String Telefone = String.valueOf(DAOBanco
					.retornaArrayClientesFisicos().get(i).getTelefone());
			String Endereco = DAOBanco.retornaArrayClientesFisicos().get(i)
					.getEndereco();
			String CEP = DAOBanco.retornaArrayClientesFisicos().get(i).getCep();

			Object[] data = { Nome, CPF, Renda, Telefone, Endereco, CEP };
			return data;
		}
		return null;
	}

	// FIM DOS METODOS CLIENTE FISICO

	// INICIO DOS METODOS CLIENTE JURIDICO
	public static void cadastrarClienteJuridico(String nome, String endereco,
			String cep, int telefone2, double renda2, int cnpj) {
		dc.cadastraPessoaJuridica(nome, endereco, cep, telefone2, renda2, cnpj);
	}

	public static void converteEcadastraJuridico(String nome, String endereco,
			String cep, String telefone, String renda, String cnpj) {
		double renda2;
		renda2 = Double.parseDouble(renda);
		int cnpj2, telefone2;
		cnpj2 = Integer.parseInt(cnpj);
		telefone2 = Integer.parseInt(telefone);
		cadastrarClienteJuridico(nome, endereco, cep, telefone2, renda2, cnpj2);

	}

	public void converteEalteraJuridico(String nome, String endereco,
			String cep, String telefone, String renda, String cnpj) {
		double renda2;
		renda2 = Double.parseDouble(renda);
		int cnpj2, telefone2;
		cnpj2 = Integer.parseInt(cnpj);
		telefone2 = Integer.parseInt(telefone);
		dc.atualizaClienteJuridico(cnpj2, dc.retornaCliente(nome, endereco,
				cep, telefone2, renda2, cnpj2));
	}

	public void removeCJuridico(int cnpj) {
		dc.removeClienteJuridico(cnpj);
	}

	public static Object mandaArrayCJparaView() {
		for (int i = 0; i < dc.getClientesJuridicos().size(); i++) {
			String Nome = DAOBanco.retornaArrayClientesJuridicos().get(i)
					.getNome();
			String CNPJ = String.valueOf(DAOBanco
					.retornaArrayClientesJuridicos().get(i).getCnpj());
			String Renda = String.valueOf(DAOBanco
					.retornaArrayClientesJuridicos().get(i).getRenda());
			String Telefone = String.valueOf(DAOBanco
					.retornaArrayClientesJuridicos().get(i).getTelefone());
			String Endereco = DAOBanco.retornaArrayClientesJuridicos().get(i)
					.getEndereco();
			String CEP = DAOBanco.retornaArrayClientesJuridicos().get(i)
					.getCep();

			Object[] data = { Nome, CNPJ, Renda, Telefone, Endereco, CEP };
			return data;
		}
		return null;
	}

	// FIM DOS METODOS CLIENTE JURIDICO

	// INICIO DOS METODOS CONTA CORRENTE

	public static void sacaDaContaCorrente(double valor, int senha, int nmro) {
		dco.sacaDaContaCorrente(valor, senha, nmro);
	}

	public static void depositaDaContaCorrente(double valor, int senha, int nmro) {
		dco.depositaDaContaCorrente(valor, senha, nmro);
	}

	public void encerraContaCorrente(int senha, int nmro) {
		dco.encerraContaCorrente(senha, nmro);
	}

	public static boolean acessaContaCorrente(int nmro, int senha) {
		return dco.validaContaCorrente(nmro, senha);
	}

	public static void criaContaCorrente(int nmro, boolean situacao,
			double saldo, Date dataAbertura, int senha, String identificacao) {
		dco.criaContaCorrente(nmro, situacao, saldo, dataAbertura, senha,
				identificacao);
	}

	public static boolean converteEcadastraCC(String identificacao,
			String senha, String saldo, String cliente) {
		int identi = Integer.parseInt(identificacao);
		int senha2 = Integer.parseInt(senha);
		double saldo2 = Double.parseDouble(saldo);
		if (cliente == "fisico") {
			for (int i = 0; i < retornaArrayClientesFisicos().size(); i++) {
				if (retornaArrayClientesFisicos().get(i).getCpf() == identi) {
					criaContaCorrente(0, true, saldo2, null, senha2,
							identificacao);
					return true;
				}
			}
		}
		if (cliente == "juridico") {
			for (int i = 0; i < retornaArrayClientesJuridicos().size(); i++) {
				if (retornaArrayClientesJuridicos().get(i).getCnpj() == identi) {
					criaContaCorrente(0, true, saldo2, null, senha2,
							identificacao);
					return true;
				}
			}

		}
		return false;
	}

	public static double retornaSaldoContaCorrente(int nmro) {
		return dco.retornaSaldoConta(nmro);
	}

	// FIM METODOS CONTA CORRENTE

	// INICIO METODOS CONTA ESPECIAL

	public static void criaContaEspecial(int nmro, boolean situacao,
			double saldo, Date dataAbertura, int senha, String identificacao,
			double limite) {
		dco.criaContaEspecial(nmro, situacao, saldo, dataAbertura, senha,
				identificacao, limite);
	}

	public static boolean converteEcadastraCE(String identificacao,
			String senha, String saldo, String limite, String cliente) {
		int identi = Integer.parseInt(identificacao);
		int senha2 = Integer.parseInt(senha);
		double saldo2 = Double.parseDouble(saldo);
		double limite2 = Double.parseDouble(limite);
		if (cliente == "fisico") {
			for (int i = 0; i < retornaArrayClientesFisicos().size(); i++) {
				if (retornaArrayClientesFisicos().get(i).getCpf() == identi) {
					criaContaEspecial(0, true, saldo2, null, senha2,
							identificacao, limite2);
					return true;
				}
			}
		}
		if (cliente == "juridico") {
			for (int i = 0; i < retornaArrayClientesJuridicos().size(); i++) {
				if (retornaArrayClientesJuridicos().get(i).getCnpj() == identi) {
					criaContaEspecial(0, true, saldo2, null, senha2,
							identificacao, limite2);
					return true;
				}
			}

		}
		return false;
	}

	public static void sacaDaContaEspecial(double valor, int senha, int nmro) {
		dco.sacaDaContaEspecial(valor, senha, nmro);
	}

	public static void depositaDaContaEspecial(double valor, int senha, int nmro) {
		dco.depositaDaContaEspecial(valor, senha, nmro);
	}

	public void encerraContaEspecial(int senha, int nmro) {
		dco.encerraContaEspecial(senha, nmro);
	}

	public void alteraLimiteDaContaEspecial(double limite, int senha, int nmro) {
		dco.alteraLimiteContaEspecial(limite, senha, nmro);
	}

	public static double retornaSaldoContaEspecial(int nmro) {
		return dco.retornaSaldoContaEspecial(nmro);
	}

	public static double retornaSaldoContaEspecialComLimite(int nmro) {
		return dco.retornaSaldoContaEspecialComLimite(nmro);
	}

	public static boolean acessaContaEspecial(int nmro, int senha) {
		if (dco.acessaContaEspecial(nmro, senha) == true) {
			return true;
		} else {
			return false;
		}
	}

	// FIM DOS METODOS CONTA CORRENTE ESPECIAL

	// INICIO DOS METODOS DA CONTA POUPAN�A
	public static void criaContaPoupanca(int nmro, boolean situacao,
			double saldo, Date dataAbertura, int senha, String identificacao) {
		dco.criaContaPoupanca(nmro, situacao, saldo, dataAbertura, senha,
				identificacao);
	}

	public static boolean converteEcadastraCP(String identificacao,
			String senha, String saldo, String cliente) {
		int identi = Integer.parseInt(identificacao);
		int senha2 = Integer.parseInt(senha);
		double saldo2 = Double.parseDouble(saldo);
		if (cliente == "fisico") {
			for (int i = 0; i < retornaArrayClientesFisicos().size(); i++) {
				if (retornaArrayClientesFisicos().get(i).getCpf() == identi) {
					criaContaPoupanca(0, true, saldo2, null, senha2,
							identificacao);
					return true;
				}
			}
		}
		if (cliente == "juridico") {
			for (int i = 0; i < retornaArrayClientesJuridicos().size(); i++) {
				if (retornaArrayClientesJuridicos().get(i).getCnpj() == identi) {
					criaContaCorrente(0, true, saldo2, null, senha2,
							identificacao);
					return true;
				}
			}

		}
		return false;
	}

	// METODOS PARA CALCULAR JUROS DO DIA DE TODAS AS CONTAS POUPAN�A
	public static void calculaEGravaJurosPoupanca(String juros) {
		for (int i = 0; i < retornaArrayContasPoupancas().size(); i++) {
			if (retornaArrayContasPoupancas().get(i).getSaldo() > 0) {
				double Saldo = retornaArrayContasPoupancas().get(i).getSaldo();
				double juros2 = Double.parseDouble(juros);
				juros2 = (Saldo * juros2) / 100;
				Saldo = Saldo + juros2;
				System.out.print(retornaArrayContasPoupancas().get(i)
						.getNumeroConta());
				System.out.println(" juros calculados " + juros2
						+ " saldo atual " + Saldo);
				// DAOConta.escreveVetorContaPoupancaComJuros(Saldo);
			}
		}
	}

	public static void sacaDaContaPoupanca(double valor, int senha, int nmro) {
		dco.sacaDaContaPoupanca(valor, senha, nmro);
	}

	public static void depositaDaContaPopanca(double valor, int senha, int nmro) {
		dco.depositaDaContaPoupanca(valor, senha, nmro);
	}

	public void encerraContaPoupanca(int senha, int nmro) {
		dco.encerraContaPoupanca(senha, nmro);
	}

	public static boolean acessaContaPoupanca(int nmro, int senha) {
		if (dco.acessaContaPoupanca(nmro, senha) == true) {
			return true;
		} else {
			return false;
		}
	}

	public static double retornaSaldoContaPoupanca(int nmro) {
		return dco.retornaSaldoContaPoupanca(nmro);
	}

	// FIM DOS METODOS DA CONTA POUPAN�A


	
	//	M�todos de Extrato a seguir...
	 
//	Devolve uma String com os movimentos de uma conta Corrente.
	public String extratoCC(int numero, int senha, Date inicio, Date fim) {
		return dco.extratoContaCorrente(numero, senha, inicio, fim);
	}
//	Devolve uma String com os movimentos de uma conta Especial.
	public String extratoCE(int numero, int senha, Date inicio, Date fim) {
		return dco.extratoContaEspecial(numero, senha, inicio, fim);
	}
//	Devolve uma String com os movimentos de uma conta Poupanca
	public String extratoCP(int numero, int senha, Date inicio, Date fim) {
		return dco.extratoContaPoupanca(numero, senha, inicio, fim);
	}
//	Grava um arquivo de texto com os movimentos de conta Corrente
	public void gravaExtratoCC(int numero, int senha, Date inicio, Date fim, String arquivo){
		dco.gravarExtratoContaCorrente(numero, senha, inicio, fim, arquivo);
	}
//	Grava um arquivo de texto com os movimentos de uma conta Especial.
	public void gravaExtratoCE(int numero, int senha, Date inicio, Date fim, String arquivo){
		dco.gravarExtratoContaEspecial(numero, senha, inicio, fim, arquivo);
	}
//	Grava um arquivo de texto com os movimentos de uma conta Poupanca.
	public void gravaExtratoCp(int numero, int senha, Date inicio, Date fim, String arquivo){
		dco.gravarExtratoContaPoupanca(numero, senha, inicio, fim, arquivo);
	}
//	M�todos de grava��o de saldo em texto plano.
	
	public void gravaSaldoContaCorrente(int numero, int senha, String arquivo){
		dco.gravaSaldoCC(numero, senha,arquivo);
	}
	
	public void gravaSaldoContaEspecial(int numero, int senha, String arquivo){
		dco.gravaSaldoCE(numero, senha, arquivo);
	}
	
	public void gravaSaldoContaPoupanca(int numero, int senha, String arquivo){
		dco.gravaSaldoCP(numero, senha, arquivo);
	}
}
