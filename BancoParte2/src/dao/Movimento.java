package dao;

import java.io.Serializable;
import java.util.Date;

public class Movimento implements Serializable {
	Date dataMovimento;
	double valor;
	String Operacao;
	
	public Movimento(double valor, String op){
		this.valor = valor;
		this.dataMovimento = new Date();
		this.Operacao = op;	
		
	}

	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getOperacao() {
		return Operacao;
	}

	public void setOperacao(String operacao) {
		Operacao = operacao;
	}
	
	public String toString(){
		String s = "";
		s += "Data: "
				+getDataMovimento()
				+ "\n" + "Opera��o: "
				+ getOperacao() + "\n"
				+ "Valor da opera��o: "
				+ getValor() + "\n\n";
		
		return s;
	}
	
}
