package dao;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import model.ContaCorrente;
import model.ContaEspecial;
import model.ContaPoupanca;

public class DAOConta {

	// metodo para gerar numero da conta corrente aleatorio e sem repeti�ao

	// INICIO DAO CONTA CORRENTE - METODOS
	private static List<ContaCorrente> contasCorrentes = new ArrayList<ContaCorrente>();

	public static List<ContaCorrente> getContasCorrentes() {
		return contasCorrentes;
	}

	public void addContaCorrente(ContaCorrente cc) {
		contasCorrentes.add(cc);
		System.out.println("ADD: " + cc);
		escreveVetorContaCorrente();
		System.out.println(getContasCorrentes());
	}

	public void atualizaContaCorrente(int nmro, ContaCorrente cc) {
		for (int i = 0; i < contasCorrentes.size(); i++) {
			if (contasCorrentes.get(i).getNumeroConta() == nmro) {
				contasCorrentes.set(i, cc);
				escreveVetorContaCorrente();
			}
		}
	}

	public void removeContaCorrente(ContaCorrente cc) {
		contasCorrentes.remove(cc);
		escreveVetorContaCorrente();
	}

	public static void escreveVetorContaCorrente() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream(
					"C:/arquivosRP/contasCorrentes.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(contasCorrentes);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void recuperaVetorContaCorrente() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream(
					"C:/arquivosRP/contasCorrentes.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			contasCorrentes = (ArrayList<ContaCorrente>) objLeitura
					.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sacaDaContaCorrente(double valor, int senha, int nmro) {
		for (ContaCorrente conta : contasCorrentes) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.saca(valor, senha) == true)) {
				Movimento movimento = new Movimento(valor, "Saque");
				conta.getMovimentos().insereFim(movimento);
				atualizaContaCorrente(nmro, conta);
				// conta.getExtrato().exibeItens();
			}
		}
	}

	public void depositaDaContaCorrente(double valor, int senha, int nmro) {
		for (ContaCorrente conta : contasCorrentes) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.deposita(valor, senha) == true)) {
				Movimento movimento = new Movimento(valor, "Deposito");
				conta.getMovimentos().insereFim(movimento);
				conta.getMovimentos().exibeItens();
				atualizaContaCorrente(nmro, conta);
			}
		}
	}

	public void encerraContaCorrente(int senha, int nmro) {
		for (ContaCorrente conta : contasCorrentes) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.encerraConta(senha) == true)) {
				atualizaContaCorrente(nmro, conta);
			}
		}
	}

	public void criaContaCorrente(int nmro, boolean situacao, double saldo,
			Date dataAbertura, int senha, String identificacao) {
		Random random = new Random();
		int nmroConta = random.nextInt(200000) + 100000;
		nmro = nmroConta;
		ContaCorrente cc = new ContaCorrente(nmro, situacao, saldo,
				dataAbertura, senha, identificacao);
		addContaCorrente(cc);
	}

	public boolean validaContaCorrente(int nmro, int senha) {
		for (int i = 0; i < contasCorrentes.size(); i++) {
			if ((contasCorrentes.get(i).getNumeroConta() == nmro)
					&& (contasCorrentes.get(i).getSenha() == senha)) {
				return true;
			}
		}
		return false;
	}

	public double retornaSaldoConta(int nmro) {
		for (ContaCorrente conta : contasCorrentes) {
			if (conta.getNumeroConta() == nmro) {
				return conta.getSaldo();
			}
		}
		return 0;
	}

	public void imprimeArrayContaCorrente() {
		for (int i = 0; i < contasCorrentes.size(); i++) {
			System.out.println("Imprimindo Array: "
					+ contasCorrentes.get(i).getNumeroConta() + " "
					+ contasCorrentes.get(i).getSenha() + " "
					+ +contasCorrentes.get(i).getSaldo());
		}
	}

	// FIM DAO CONTA CORRENTE - METODOS

	// INICIO DAO CONTA CORRENTE ESPECIAL - METODOS

	private static List<ContaEspecial> contasEspeciais = new ArrayList<ContaEspecial>();

	public static List<ContaEspecial> getContasEspeciais() {
		return contasEspeciais;
	}

	public void addContaEspecial(ContaEspecial ce) {
		contasEspeciais.add(ce);
		escreveVetorContaEspecial();
	}

	public void atualizaContaEspecial(int nmro, ContaEspecial ce) {
		for (int i = 0; i < contasEspeciais.size(); i++) {
			if (contasEspeciais.get(i).getNumeroConta() == nmro) {
				contasEspeciais.set(i, ce);
				escreveVetorContaEspecial();
			}
		}
	}

	public void removeContaEspecial(ContaEspecial cce) {
		contasEspeciais.remove(cce);
		escreveVetorContaEspecial();
	}

	public static void escreveVetorContaEspecial() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream(
					"C:/arquivosRP/contasEspeciais.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(contasEspeciais);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetorContaEspecial() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream(
					"C:/arquivosRP/contasEspeciais.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			contasEspeciais = (ArrayList<ContaEspecial>) objLeitura
					.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sacaDaContaEspecial(double valor, int senha, int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.saca(valor, senha) == true)) {
				atualizaContaEspecial(nmro, conta);
			}
		}
	}

	public void depositaDaContaEspecial(double valor, int senha, int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.deposita(valor, senha) == true)) {
				atualizaContaEspecial(nmro, conta);
			}
		}

	}

	public void encerraContaEspecial(int senha, int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.encerraConta(senha) == true)) {
				atualizaContaEspecial(nmro, conta);
			}
		}
	}

	public void criaContaEspecial(int nmro, boolean situacao, double saldo,
			Date dataAbertura, int senha, String identificacao, double limite) {
		Random random = new Random();
		int nmroConta = random.nextInt(200000) + 300000;
		nmro = nmroConta;
		ContaEspecial cce = new ContaEspecial(nmro, situacao, saldo,
				dataAbertura, senha, identificacao, limite);
		addContaEspecial(cce);
	}

	public double retornaSaldoContaEspecial(int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if (conta.getNumeroConta() == nmro) {
				return conta.getSaldo();
			}
		}
		return 0;
	}

	public void alteraLimiteContaEspecial(double limite, int senha, int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if (conta.alteraLimite(limite, senha) == true) {
				atualizaContaEspecial(nmro, conta);
			}
		}
	}

	public double retornaSaldoContaEspecialComLimite(int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if (conta.getNumeroConta() == nmro) {
				return conta.getSaldo() + conta.getLimite();

			}
		}
		return 0;
	}

	public boolean acessaContaEspecial(int nmro, int senha) {
		for (int i = 0; i < contasEspeciais.size(); i++) {
			if ((contasEspeciais.get(i).getNumeroConta() == nmro)
					&& (contasEspeciais.get(i).getSenha() == senha)) {
				return true;
			}
		}
		return false;

	}

	// FIM DAO CONTA CORRENTE ESPECIAL - METODOS

	// INICIO CONTA POUPANCA - METODOS

	private static List<ContaPoupanca> contasPoupanca = new ArrayList<ContaPoupanca>();

	public static List<ContaPoupanca> getContasPoupanca() {
		return contasPoupanca;
	}

	public void addConta(ContaPoupanca p) {
		contasPoupanca.add(p);
		escreveVetorContaPoupanca();
	}

	public void atualizaConta(int nmro, ContaPoupanca cp) {
		for (int i = 0; i < contasPoupanca.size(); i++) {
			if (contasPoupanca.get(i).getNumeroConta() == nmro) {
				contasPoupanca.set(i, cp);
				escreveVetorContaPoupanca();
			}
		}
	}

	public void removeConta(ContaPoupanca p) {
		contasPoupanca.remove(p);
		escreveVetorContaPoupanca();
	}

	public static void escreveVetorContaPoupanca() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream(
					"C:/arquivosRP/contasPoupanca.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(contasPoupanca);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetorContaPoupanca() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream(
					"C:/arquivosRP/contasPoupanca.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			contasPoupanca = (ArrayList<ContaPoupanca>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sacaDaContaPoupanca(double valor, int senha, int nmro) {
		for (ContaPoupanca conta : contasPoupanca) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.saca(valor, senha) == true)) {
				atualizaConta(nmro, conta);
			}
		}
	}

	public void depositaDaContaPoupanca(double valor, int senha, int nmro) {
		for (ContaPoupanca conta : contasPoupanca) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.deposita(valor, senha) == true)) {
				System.out.println(" " + conta.getSaldo());
				atualizaConta(nmro, conta);
				// System.out.println("depositou ");
			} else {
				// System.out.println("Nao depositou ");
			}
		}
	}

	public void encerraContaPoupanca(int senha, int nmro) {
		for (ContaPoupanca conta : contasPoupanca) {
			if ((conta.getNumeroConta() == nmro)
					&& (conta.encerraConta(senha) == true)) {
				atualizaConta(nmro, conta);
			}
		}
	}

	public void criaContaPoupanca(int nmro, boolean situacao, double saldo,
			Date dataAbertura, int senha, String identificacao) {
		Random random = new Random();
		int nmroConta = random.nextInt(200000) + 500000;
		nmro = nmroConta;
		ContaPoupanca cp = new ContaPoupanca(nmro, situacao, saldo,
				dataAbertura, senha, identificacao);
		addConta(cp);
	}

	public boolean acessaContaPoupanca(int nmro, int senha) {
		for (int i = 0; i < contasPoupanca.size(); i++) {
			if ((contasPoupanca.get(i).getNumeroConta() == nmro)
					&& (contasPoupanca.get(i).getSenha() == senha)) {
				return true;
			}
		}
		return false;
	}

	public double retornaSaldoContaPoupanca(int nmro) {
		for (ContaPoupanca conta : contasPoupanca) {
			if (conta.getNumeroConta() == nmro) {
				return conta.getSaldo();
			}
		}
		return 0;
	}
/*
 * Faz uma busca nos arrays de contas Contas Correntes. e retorna um objeto.
 */
	public ContaCorrente buscaContaCorrente(int numero, int senha) throws NullPointerException{
		for (ContaCorrente conta : contasCorrentes) {
			if (conta.getNumeroConta() == numero && conta.getSenha() == senha) {
				return conta;
			}
		}
		throw new NullPointerException("N�mero da Conta ou senha Invalidos!");
	}
	/*
	 * Faz uma busca nos arrays de contas Contas Especiais. e retorna um objeto.
	 */
	public ContaEspecial buscaContaEspecial(int numero, int senha) throws NullPointerException{
		for (ContaEspecial contaE : contasEspeciais) {
			if (contaE.getNumeroConta() == numero && contaE.getSenha() == senha) {
				return contaE;
			}

		}
		throw new NullPointerException("N�mero da Conta ou senha Invalidos!");
	}
	/*
	 * Faz uma busca nos arrays de contas Contas Poupan�a. e retorna um objeto.
	 */
	public ContaPoupanca buscaContaPoupanca(int numero, int senha) throws NullPointerException {
		for (ContaPoupanca contaP : contasPoupanca) {
			if (contaP.getNumeroConta() == numero && contaP.getSenha() == senha) {
				return contaP;
			}
		}
		throw new NullPointerException("N�mero da Conta ou senha Invalidos!");
	}
//	M�todo que retorna uma string com o extrato de uma conta Corrente dentro de um intervalo de tempo
	public String extratoContaCorrente(int numero, int senha, Date inicio, Date fim) {
		ContaCorrente aux = buscaContaCorrente(numero, senha);
		return aux.montaExtrato(inicio, fim);
	}
//	M�todo que retorna uma string com o extrato de uma conta Especial dentro de um intervalo de tempo
	public String extratoContaEspecial(int numero, int senha, Date inicio, Date fim) {
		ContaEspecial aux = buscaContaEspecial(numero, senha);
		return aux.montaExtrato(inicio, fim);
	}
//	M�todo que retorna uma string com o extrato de uma conta Poupan�a dentro de um intervalo de tempo
	public String extratoContaPoupanca(int numero, int senha, Date inicio, Date fim) {
		ContaPoupanca aux = buscaContaPoupanca(numero, senha);
		return aux.montaExtrato(inicio, fim);
	}
// 	Grava um extrato em texto plano de uma conta Corrente.
	public void gravarExtratoContaCorrente(int numero, int senha, Date inicio, Date fim, String arquivo) {
		escreveArquivo(extratoContaCorrente(numero, senha, inicio, fim), arquivo);
	}
//	Grava um extrato em texto plano de uma conta Especial
	public void gravarExtratoContaEspecial(int numero, int senha, Date inicio, Date fim, String arquivo) {
		escreveArquivo(extratoContaEspecial(numero, senha, inicio, fim), arquivo);
	}
//	Grava um extrato em texto plano de uma conta Poupan�as
	public void gravarExtratoContaPoupanca(int numero, int senha, Date inicio, Date fim, String arquivo) {
		escreveArquivo(extratoContaPoupanca(numero, senha, inicio, fim), arquivo);
	}
	
//	Grava o saldo de uma conta em arquivo texto.
	public void gravaSaldoCC(int numero, int senha, String arquivo){
		ContaCorrente aux = buscaContaCorrente(numero, senha);
		String s = "";
		s += "N�mero da Conta: " + aux.getNumeroConta() + "\n" 
				+ "Saldo: " + aux.getSaldo() + "\n";
		escreveArquivo(s, arquivo);
	}
	
//	Grava o saldo de uma conta em arquivo texto.
	public void gravaSaldoCE(int numero, int senha, String arquivo){
		ContaEspecial aux = buscaContaEspecial(numero, senha);
		String s = "";
		s += "N�mero da Conta: " + aux.getNumeroConta() + "\n" 
				+ "Saldo: " + aux.getSaldo() + "\n";
		escreveArquivo(s, arquivo);
	}
	
//	Grava o saldo de uma conta em arquivo texto.
	public void gravaSaldoCP(int numero, int senha, String arquivo){
		ContaPoupanca aux = buscaContaPoupanca(numero, senha);
		String s = "";
		s += "N�mero da Conta: " + aux.getNumeroConta() + "\n" 
				+ "Saldo: " + aux.getSaldo() + "\n";
		escreveArquivo(s,arquivo);
	}
//	Recebe uma String e Grava em arquivo de texto.
	public void escreveArquivo(String s, String arquivo) {
		Path path = Paths.get("C:/arquivosRP/" + arquivo);

		BufferedWriter escritor;
		try {
			escritor = Files.newBufferedWriter(path);
			escritor.write(s);
			escritor.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}