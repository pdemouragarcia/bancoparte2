package dao;

import java.io.Serializable;
import java.util.Date;

public class ListaLigada implements Serializable {
	No inicio;

	public ListaLigada() {
		this.inicio = null;
	}

	// Verifica se a lista esta vazia
	public boolean ehVazia() {
		if (this.inicio == null)
			return true;
		else
			return false;
	}

	// Insere movimento no inicio
	public void insereInicio(Movimento movimento) {
		No aux = new No(movimento, this.inicio);
		this.inicio = aux;
	}

	// Remove movimento do inicio da lista
	public Movimento removeInicio() throws Exception {
		Movimento movimento;
		if (!this.ehVazia()) {
			movimento = this.inicio.getMovimento();
			this.inicio = this.inicio.getProx();
			return movimento;
		} else
			throw new Exception(
					"Lista ligada vazia. N�o � poss�vel remover item.");
	}

	// Insere movimento no fim da lista
	public void insereFim(Movimento movimento) {
		No aux = this.inicio;

		if (!this.ehVazia()) {
			while (aux.getProx() != null)
				aux = aux.getProx();
			aux.setProx(new No(movimento, null));
		} else
			this.inicio = new No(movimento, null);
	}

	// Exibe itens da lista
	public void exibeItens() {
		No aux = this.inicio;
		if (!this.ehVazia()) {
			while (aux != null) {
				System.out.println("Data: "
						+ aux.getMovimento().getDataMovimento() + " - Valor:  "
						+ aux.getMovimento().getValor() + " - Opera��o: "
						+ aux.getMovimento().getOperacao());
				aux = aux.getProx();
			}
		} else {
			System.out.println("Nenhum Movimento");
		}
	}

	public No getNo() {
		return this.inicio;
	}

	

}
