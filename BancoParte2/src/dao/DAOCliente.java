package dao;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import model.ClienteFisico;
import model.ClienteJuridico;

public class DAOCliente {
	
	// INICIO DAO CLIENTE FISICO - METODOS DE CRUD E SERIALIZAÇÃO
	private static List<ClienteFisico> clientesFisicos = new ArrayList<ClienteFisico>();

	public static List<ClienteFisico> getClientesFisicos() {
		return clientesFisicos;
	}

	public void addClienteFisico(ClienteFisico p) {
		clientesFisicos.add(p);
		JOptionPane.showMessageDialog(null, " Cadastro de Cliente Físico Realizado ", "",
				JOptionPane.INFORMATION_MESSAGE);
		escreveVetorClienteFisico();
	}

	public void atualizaClienteFisico(int cpf, ClienteFisico p) {
		for ( int i = 0; i < clientesFisicos.size(); i++) {
			if (clientesFisicos.get(i).getCpf() == cpf) {
				clientesFisicos.set(i, p);
				escreveVetorClienteFisico();
				System.out.println("Alterou");
			}
		}
	}
	public ClienteFisico retornaCliente(String nome, String endereco, String cep, int telefone2, double renda2,
			int cpf2, int rg2){
		Object clienteFisico;
		return (ClienteFisico) (clienteFisico = new ClienteFisico(nome, endereco, cep, telefone2, renda2, cpf2, rg2));
	}

	public void removeClienteFisico(int cpf) {
		clientesFisicos.remove(cpf);
		escreveVetorClienteFisico();
		}

	public void cadastraCliente(String nome, String endereco, String cep, int telefone2, double renda2, int cpf2, int rg2) {
		ClienteFisico cf = new ClienteFisico(nome, endereco, cep, telefone2, renda2, cpf2, rg2);
		addClienteFisico(cf);
	}
	
	
	public static void escreveVetorClienteFisico() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream("C:/arquivosRP/clientesFisicos.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(clientesFisicos);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetorClienteFisico() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream("C:/arquivosRP/clientesFisicos.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			clientesFisicos = (ArrayList<ClienteFisico>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void imprimeArrayClienteFisico() {
		for (int i = 0; i < clientesFisicos.size(); i++) {
			System.out.println("Imprimindo Array: " + clientesFisicos.get(i).getNome() + " "
					+ clientesFisicos.get(i).getCpf() + " " + +clientesFisicos.get(i).getTelefone());
		}
	}
	// FIM DAO CLIENTE FISICO - METODOS DE CRUD E SERIALIZAÇÃO
	
	
	// INICIO DAO CLIENTE JURIDICO - METODOS DE CRUD E SERIALIZAÇÃO
	private static List<ClienteJuridico> clientesJuridicos = new ArrayList<ClienteJuridico>();

	public static List<ClienteJuridico> getClientesJuridicos() {
		return clientesJuridicos;
	}

	public void addClienteJuridico(ClienteJuridico p) {
		clientesJuridicos.add(p);
		JOptionPane.showMessageDialog(null, " Cadastro de Cliente Jurídico Realizado ", "",
				JOptionPane.INFORMATION_MESSAGE);
		escreveVetorClienteJuridico();
	}

	public void atualizaClienteJuridico(int Cnpj, ClienteJuridico p) {
		for (int i = 0; i < clientesJuridicos.size(); i++) {
			if (clientesJuridicos.get(i).getCnpj() == Cnpj) {
				clientesJuridicos.set(i, p);
				escreveVetorClienteJuridico();
				System.out.println("Alterou");
			}
		}
	}
	
	public ClienteJuridico retornaCliente(String nome, String endereco, String cep, int telefone2, double renda2,
			int cnpj2){
		Object cliente;
		return (ClienteJuridico) (cliente = new ClienteJuridico(nome, endereco, cep, telefone2, renda2, cnpj2));
	}

	public void removeClienteJuridico(int cnpj) {
		clientesJuridicos.remove(cnpj);
		escreveVetorClienteJuridico();
	}

	public static void escreveVetorClienteJuridico() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream("C:/arquivosRP/clientesJuridicos.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(clientesJuridicos);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetorClienteJuridico() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream("C:/arquivosRP/clientesJuridicos.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			clientesJuridicos = (ArrayList<ClienteJuridico>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cadastraPessoaJuridica(String nome, String endereco, String cep, int telefone2, double renda2,
			int cnpj) {
		ClienteJuridico cj = new ClienteJuridico(nome, endereco, cep, telefone2, renda2, cnpj);
		addClienteJuridico(cj);
	}
	
	// FIM DAO CLIENTE JURIDICO - METODOS DE CRUD E SERIALIZAÇÃO

}
