package dao;

import java.io.Serializable;
import java.util.Date;

public class No implements Serializable{
	private No prox;
	private Movimento movimento;

	public No() {
		this.prox = null;
	}
	
	public No(Movimento mov, No prox) {
		this.movimento = mov;
		this.prox = prox;
		int numeroConta;
		Date dataAbertura;
		Date dataEncerramento;
		boolean aberta;
		int senha;
		String identificacao;
		double saldo;
	}
	
	public void setMovimento(Movimento movimento) {
		this.movimento = movimento;
	}

	public Movimento getMovimento() {
		return this.movimento;
	}

	public void setProx(No prox) {
		this.prox = prox;
	}

	public No getProx() {
		return this.prox;
	}
}
